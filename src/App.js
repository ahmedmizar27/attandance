import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './component/login/Login'
import Attendance from './component/Attendance/Attendance';

class App extends Component {
  state = {
    loggedIn: false,
  };
  render() {
    
    return (
      <Router>
        <div className="App">
          <Switch>
          <Route exact path='/login'  component={Login} />
          <Route exact path='/'  component={Login} />
          <Route exact path='/attendance'  component={Attendance} />
          </Switch>
          
        </div>
      </Router>
    );
  }
}

export default App;
