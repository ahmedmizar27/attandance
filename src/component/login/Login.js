import React, { Component } from "react";
import "./Login.css";
import { Redirect } from "react-router-dom";
export class Login extends Component {
  state = {
    email: "",
    password: "",
    isLogin: false,
    errorTXT: ""
  };
  handleChangeEmail = event => {
    this.setState({
      email: event.target.value
    });
  };
  handleChangePassword = event => {
    this.setState({
      password: event.target.value
    });
  };
  handleSubmit = event => {
    event.preventDefault();
    console.log(this.state);
    const { email, password } = this.state;
    if (email === "admin@admin.com" && password === "password") {
      localStorage.setItem("token", this.state);
      this.setState({ errorTXT: "", isLogin: true });
    } else if (this.state.password.length < 8) {
      this.setState({
        errorTXT: "The password length should be 8 characters minimum.",
        isLogin: false
      });
    } else {
      this.setState({
        errorTXT: "you got the wrong credentials",
        isLogin: false
      });
    }
  };

  render() {
    if (this.state.isLogin) {
      return <Redirect to="/attendance" />;
    }
    return (
      <div className="loginContainer">
        <div className="loginHeader">
          <h1>Login</h1>
          <p>
            Join Thousands of Companies That Use <br />
            You in Every Day!
          </p>
        </div>
        <div className="formContainer">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label>E-mail</label>
              <input
                type="email"
                className="form-control"
                placeholder="Enter"
                name="email"
                value={this.state.email}
                onChange={this.handleChangeEmail}
              />
            </div>
            <div className="form-group">
              <label>Password:</label>
              <input
                type="password"
                className="form-control"
                placeholder="Enter password"
                name="password"
                value={this.state.password}
                onChange={this.handleChangePassword}
              />
            </div>
            {this.state.errorTXT && this.state.errorTXT.length > 0 && (
              <div className="error">
                <p>{this.state.errorTXT}</p>
              </div>
            )}

            <div>Forget Password!</div>
            <div className="loginBtn">
              <button type="submit">Login</button>
            </div>
            <p>
              Already User?<a href="#">Sign in now</a>
            </p>
          </form>
        </div>
      </div>
    );
  }
}

export default Login;
