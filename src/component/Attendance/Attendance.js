import React, { Component } from "react";
import "./Attendance.css";
import myData from "../../attendance_data.js";
import { Redirect } from "react-router-dom";

class Attendance extends Component {
  constructor(props) {
    super(props);
    const token = localStorage.getItem("token");
    let loggedIn = true;
    if (token == null) {
      loggedIn = false;
    }
    this.state = {
      AllRecourses: null,
      CheckIn: null,
      CheckOut: null,
      AllRecoursesPrimaryArr: null,
      CheckInPrimaryArr: null,
      CheckOutPrimaryArr: null,
      searchTxt: "",
      searchTxtCheckIn: "",
      searchTxtCheckOut: "",
      loggedIn
    };
  }

  componentDidMount() {
    this.filterData();
  }

  filterData() {
    console.log(myData.data);
    var AllRecourses, CheckIn, CheckOut;
    AllRecourses = myData.data;
    CheckIn = AllRecourses.filter(Item => {
      return Item.checked_in_at === true;
    });
    CheckOut = AllRecourses.filter(Item => {
      return Item.checked_out_at === true;
    });
    this.setState({
      AllRecourses: AllRecourses,
      AllRecoursesPrimaryArr: AllRecourses,
      CheckIn: CheckIn,
      CheckOut: CheckOut
    });
  }

  _searchOnChangeTextAll(e) {
    var tempArr;
    var val = e.target.value;
    this.setState({ searchTxt: e.target.value }, () => {
      if (this.state.searchTxt.length > 0) {
        if (val) {
          tempArr = this.state.AllRecoursesPrimaryArr.filter(item => {
            return (
              item.resource.first_name
                .toLowerCase()
                .indexOf(val.toLowerCase()) > -1 ||
              item.resource.last_name.toLowerCase().indexOf(val.toLowerCase()) >
                -1
            );
          });
          this.setState({
            AllRecourses: tempArr
          });
        }
      } else {
        this.setState({
          AllRecourses: this.state.AllRecoursesPrimaryArr
        });
      }
    });
  }

  _searchOnChangeTextCheckIn(e) {
    var tempArr;
    var val = e.target.value;
    this.setState({ searchTxtCheckIn: e.target.value }, () => {
      if (this.state.searchTxtCheckIn.length > 0) {
        if (
          val &&
          this.state.CheckInPrimaryArr &&
          this.state.CheckInPrimaryArr.length > 0
        ) {
          tempArr = this.state.CheckInPrimaryArr.filter(item => {
            return (
              item.resource.first_name
                .toLowerCase()
                .indexOf(val.toLowerCase()) > -1 ||
              item.resource.last_name.toLowerCase().indexOf(val.toLowerCase()) >
                -1
            );
          });
          this.setState({
            CheckIn: tempArr
          });
        }
      } else {
        this.setState({
          CheckIn: this.state.CheckInPrimaryArr
        });
      }
    });
  }

  changeSearchTxtCheckOut(e) {
    var tempArr;
    var val = e.target.value;
    this.setState({ searchTxtCheckOut: e.target.value }, () => {
      if (this.state.searchTxtCheckOut.length > 0) {
        if (
          val &&
          this.state.CheckOutPrimaryArr &&
          this.state.CheckOutPrimaryArr.length > 0
        ) {
          tempArr = this.state.CheckOutPrimaryArr.filter(item => {
            return (
              item.resource.first_name
                .toLowerCase()
                .indexOf(val.toLowerCase()) > -1 ||
              item.resource.last_name.toLowerCase().indexOf(val.toLowerCase()) >
                -1
            );
          });
          this.setState({
            CheckOut: tempArr
          });
        }
      } else {
        this.setState({
          CheckOut: this.state.CheckOutPrimaryArr
        });
      }
    });
  }

  CheckIn(index) {
    var temp = this.state.AllRecourses[index];
    var temp0 = this.state.CheckIn.filter(Item => {
      return Item.id === temp.id;
    });
    var temp1 = this.state.CheckOut.filter(Item => {
      return Item.id === temp.id;
    });
    if (temp0.length === 0 && temp1.length === 0) {
      this.setState({
        CheckIn: this.state.CheckIn.concat(temp),
        CheckInPrimaryArr: this.state.CheckIn.concat(temp)
      });
    }
  }

  CheckOut(Item, index) {
    this.state.CheckIn.splice(index, 1);
    this.setState({
      CheckOut: this.state.CheckOut.concat(Item),
      CheckOutPrimaryArr: this.state.CheckOut.concat(Item)
    });
  }

  Delete(type, index) {
    var temp;
    if (type === "All") {
      temp = this.state.AllRecourses.filter(Item => {
        return Item.id !== index;
      });
      this.setState({ AllRecourses: temp, AllRecoursesPrimaryArr: temp });
    } else if (type === "CheckIn") {
      temp = this.state.CheckIn.filter(Item => {
        return Item.id !== index;
      });
      this.setState({ CheckIn: temp, CheckInPrimaryArr: temp });
    } else {
      temp = this.state.CheckOut.filter(Item => {
        return Item.id !== index;
      });
      this.setState({ CheckOut: temp, CheckOutPrimaryArr: temp });
    }
  }

  render() {
    if (this.state.loggedIn === false) {
      return <Redirect to="/" />;
    }
    return (
      <div className="AttendanceContainer">
        <p className="AttendanceTxt">Attendance</p>
        <div className="row">
          <div className="col-md-4">
            <div className="Box">
              <div className="input-group">
                <input
                  className="form-control  border-right-0 "
                  type="search"
                  value={this.state.searchTxt}
                  id="example-search-input"
                  placeholder="All Resources"
                  onChange={this._searchOnChangeTextAll.bind(this)}
                />
                <span className="input-group-append">
                  <div className="input-group-text bg-transparent">
                    <i className="fas fa-search"></i>
                  </div>
                </span>
              </div>
              {this.state.AllRecourses &&
                this.state.AllRecourses.map((Item, key) => {
                  return (
                    <div className="Card" key={key}>
                      <div className="row ">
                        <div className="col-md-3">
                          <img
                            src={Item.resource.profile_picture}
                            className="profilePic"
                          />
                        </div>
                        <div className="col-md-9 noPaddingLeft">
                          <p className="Name">
                            {Item.resource.first_name +
                              " " +
                              Item.resource.last_name}
                          </p>
                          <p className="Title">
                            {Item.resource.types.map(type => {
                              return type.name + "  ";
                            })}
                          </p>
                          <p className="Details">
                            {Item.resource.phone_number +
                              " " +
                              Item.resource.email}
                          </p>
                        </div>
                        <div className="dropdown">
                          <div
                            className="test"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          ></div>
                          <div
                            className="dropdown-menu"
                            aria-labelledby="dropdownMenuButton"
                          >
                            <div className="dropdownMenuContainer">
                              <i className="far fa-check-square"></i>
                              <p
                                className="Check"
                                onClick={() => {
                                  this.CheckIn(key);
                                }}
                              >
                                Check In
                              </p>
                            </div>
                            <div className="dropdownMenuContainer">
                              <i className="fa fa-trash-alt"></i>
                              <p
                                className="Check"
                                onClick={() => this.Delete("All", Item.id)}
                              >
                                Delete
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
          <div className="col-md-4">
            <div className="Box">
              <div className="input-group">
                <input
                  className="form-control  border-right-0 "
                  type="search"
                  value=""
                  id="example-search-input"
                  placeholder="Check In"
                  onChange={this._searchOnChangeTextCheckIn.bind(this)}
                  value={this.state.searchTxtCheckIn}
                />
                <span className="input-group-append">
                  <div className="input-group-text bg-transparent">
                    <i className="fas fa-search"></i>
                  </div>
                </span>
              </div>
              {this.state.CheckIn &&
                this.state.CheckIn.map((Item, key) => {
                  return (
                    <div className="Card">
                      <div className="row ">
                        <div className="col-md-3">
                          <img
                            src={Item.resource.profile_picture}
                            className="profilePic"
                          />
                        </div>
                        <div className="col-md-9 ">
                          <p className="Name">
                            {Item.resource.first_name +
                              " " +
                              Item.resource.last_name}
                          </p>
                          <p className="Title">
                            {Item.resource.types.map(type => {
                              return type.name + "  ";
                            })}
                          </p>
                          <p className="Details">
                            {Item.resource.phone_number +
                              " " +
                              Item.resource.email}
                          </p>
                        </div>
                        <div className="dropdown">
                          <div
                            className="test"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          ></div>
                          <div
                            className="dropdown-menu"
                            aria-labelledby="dropdownMenuButton"
                          >
                            <div className="dropdownMenuContainer">
                              <i className="far fa-check-square"></i>
                              <p
                                className="Check"
                                onClick={() => this.CheckOut(Item, key)}
                              >
                                Check out
                              </p>
                            </div>
                            <div className="dropdownMenuContainer">
                              <i className="fa fa fa-trash-alt"></i>
                              <p
                                className="Check"
                                onClick={() => this.Delete("CheckIn", Item.id)}
                              >
                                Delete
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
          <div className="col-md-4">
            <div className="Box">
              <div className="input-group">
                <input
                  className="form-control  border-right-0 "
                  type="search"
                  value=""
                  id="example-search-input"
                  placeholder="Check Out"
                  onChange={this.changeSearchTxtCheckOut.bind(this)}
                  value={this.state.searchTxtCheckOut}
                />
                <span className="input-group-append">
                  <div className="input-group-text bg-transparent">
                    <i className="fas fa-search"></i>
                  </div>
                </span>
              </div>
              {this.state.CheckOut &&
                this.state.CheckOut.map((Item, key) => {
                  return (
                    <div className="Card">
                      <div className="row ">
                        <div className="col-md-3">
                          <img
                            src={Item.resource.profile_picture}
                            className="profilePic"
                          />
                        </div>
                        <div className="col-md-9 ">
                          <p className="Name">
                            {Item.resource.first_name +
                              " " +
                              Item.resource.last_name}
                          </p>
                          <p className="Title">
                            {Item.resource.types.map(type => {
                              return type.name + "  ";
                            })}
                          </p>
                          <p className="Details">
                            {Item.resource.phone_number +
                              " " +
                              Item.resource.email}
                          </p>
                        </div>
                        <div className="dropdown">
                          <div
                            className="test"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          ></div>
                          <div
                            className="dropdown-menu"
                            aria-labelledby="dropdownMenuButton"
                          >
                            <div className="dropdownMenuContainer">
                              <i className="fa fa fa-trash-alt"></i>
                              <p
                                className="Check"
                                onClick={() => this.Delete("CheckOut", Item.id)}
                              >
                                Delete
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Attendance;
